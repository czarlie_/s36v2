// Controllers contain the functions and business logics of our Express JavaScript application
// Meaning, all the operations it can do will be placed in this file
const Task = require('../models/Task')

// Controller function for getting all the tasks
// Define functions to be used in the taskRoutes.js and export these function

module.exports.getAllTasks = () =>
  // the 'return' statement, returns the result of the Mongoose method 'find' back to the taskRoutes.js
  Task.find({}).then((result) => result)

//Mini Activity Start
// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoutes.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {
  // Creates a task object based on the Mongoose model "Task"
  let newTask = new Task({
    // Sets the "name" property with the value received from the client/Postman
    name: requestBody.name,
  })

  // Saves the newly created "newTask" object in the MongoDB database
  // The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
  // The "then" method will accept the following 2 arguments:
  // The first parameter will store the result returned by the Mongoose "save" method
  // The second parameter will store the "error" object
  // Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
  // Ex.
  // newUser.save((saveErr, savedTask) => {})
  return newTask.save().then((task, error) => {
    // If an error is encountered returns a "false" boolean back to the client/Postman
    if (error) {
      console.log(error)
      // If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
      // Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
      // The else statement will no longer be evaluated
      return false

      // Save successful, returns the new task object back to the client/Postman
    } else {
      return task
    }
  })
}

//Mini Activity End

// Controller function for deleting a task
// "taskID" is the URL parameter passed from the taskRoutes.js file

// Business Logic
/*
  1) Look for the task with the corresponding id provided in the URL/route
  2) Delete the task using Mongoose method .findByIdAndRemove() with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
    if (error) {
      console.log(error)
      return false
    } else return `'${removedTask.name}' task has been deleted 🤐`
  })
}

// Controller function for updating a task
// Business Logic
/*
  1) Get a task with the id usingthe Mongoose method .findById()
  2) Replace the task's name returned from the database with the 'name' property from the request body
  3) Save the task
*/

module.exports.updateTask = (taskId, newRequestBody) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error)
      return false
    }

    result.name = newRequestBody.name
    return result.save().then((updatedTask, saveError) => {
      if (saveError) {
        console.log(saveError)
        return false
      } else return updatedTask
    })
  })
}

module.exports.getTask = (taskId) =>
  Task.findById(taskId).then((result) => result)

module.exports.updateStatus = (taskId, taskStatus) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) console.log(error)

    result.status = taskStatus.status
    return result.save().then((updatedResult, error) => {
      error ? console.log(error) : console.log(updatedResult)
    })
  })
}

// this file contains ALL the endpoints for our application
// we separate the routes such that 'index.js' only contains information on the server
// we need to use express' Router() function to achieve this
const express = require('express')

// creates a router instance that functions as our middleware and routing systems
// makes it easier to create routes for our application
const router = express.Router()

// the taskController allows us to use the functions to find in the taskController.js
const taskController = require('../controllers/taskControllers')

// Route
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// all the business logic is done in the controller

// Route to get all the tasks
// this route expects to receive a GET request at the URL '/tasks'

router.get('/', (request, response) => {
  taskController
    .getAllTasks()
    .then((resultFromController) => response.send(resultFromController))
})

//Mini Activity Start

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"
// The whole URL is at "http://localhost:3001/tasks"
router.post('/', (request, response) => {
  // The "createTask" function needs the data from the request body, so we need to supply it to the function
  // If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
  //taskController.createTask(request.body);
  taskController
    .createTask(request.body)
    .then((resultFromController) => response.send(resultFromController))
})
//Mini-Activity End

// Route to delete a task
// This route expects to receive a DELETE request at the URL "/tasks/:id"
// The whole URL is at "http://localhost:4000/tasks/:id"

router.delete('/:id', (request, response) => {
  taskController
    .deleteTask(request.params.id)
    .then((resultFromController) => response.send(resultFromController))
})

// route to update a task
// this route expects to receive a PUT request at the URL "/tasks/:id"
router.put('/:id', (request, response) => {
  taskController
    .updateTask(request.params.id, request.body)
    .then((resultFromController) => response.send(resultFromController))
})

router.get('/:id', (request, response) => {
  taskController
    .getTask(request.params.id)
    .then((resultFromController) => response.send(resultFromController))
})
router.put('/:id/complete', (request, response) => {
  taskController
    .updateStatus(request.params.id, request.body)
    .then((resultFromController) => response.send(resultFromController))
})
// module.exports to export the router object to use in the index.js
module.exports = router

// Setup the dependencies
const express = require('express')
const mongoose = require('mongoose')
// This allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require('./routes/taskRoutes')

// Setup Server
const app = express()
const port = 4000

// Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Database Connection
// Connecting to MongoDB Atlas
mongoose.set('strictQuery', true)
mongoose.connect(
  'mongodb+srv://admin:admin123@b248.ctuoffq.mongodb.net/s35?retryWrites=true&w=majority',
  {
    useNewURLParser: true,
    useUnifiedTopology: true,
  }
)

// Connection to the database
let databaseKo = mongoose.connection
databaseKo.on('error', console.error.bind(console, 'connection error'))
databaseKo.once('open', () =>
  console.log(`Hello World! 👋🌏
We are connected to MongoDB Atlas! 🥳💯`)
)

// Add the task route
// Allows all the task routes created in taskRoutes.js file to use '/tasks' route
app.use('/tasks', taskRoutes)

// Server Listening
app.listen(port, () =>
  console.log(`Now listening via port │█║▌║▌║${port}║▌║▌║█│`)
)
